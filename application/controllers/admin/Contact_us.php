<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Contact_us extends CI_Controller
  {

      public function __construct()
      {
        parent::__construct();
        $this->load->helper('url');
        
        if($this->session->userdata('status') != 'Login'){
          redirect(base_url('index.php/credential/login'));
        }
      }

      public function index()
      {
        $this->load->view('admin/page/contact_us');
      }
  }
?>