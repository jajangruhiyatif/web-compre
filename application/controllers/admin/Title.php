<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Title extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->model('m_title');
        $this->load->helper('url');
        
        if($this->session->userdata('status') != 'Login'){
          redirect(base_url('index.php/credential/login'));
        }
      }

      public function index()
      {
        $data['title'] = $this->m_title->list_title();
        $this->load->view('admin/page/title', $data);
      }

      public function show()
      {
        $data = $this->m_title->list_title();
        echo json_encode($data);
      }

      public function save()
      {
        $data = $this->m_title->save_title();
        echo json_encode($data);
      }

      public function update()
      {
        $data = $this->m_title->update_title();
        echo json_encode($data);
      }

      public function delete()
      {
        $data = $this->m_title->delete_title();
        echo json_encode($data);
      }
  }
?>