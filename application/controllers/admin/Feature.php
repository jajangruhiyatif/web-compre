<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Feature extends CI_Controller
  {

      public function __construct()
      {
        parent::__construct();
        $this->load->model('m_feature');
        $this->load->helper('url');
        
        if($this->session->userdata('status') != 'Login'){
          redirect(base_url('index.php/credential/login'));
        }
      }

      public function index()
      {
        $data['feature'] = $this->m_feature->getAll();
        $this->load->view('admin/page/feature', $data);
      }
  }
?>