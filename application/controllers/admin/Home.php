<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Home extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->helper('url');

        if($this->session->userdata('status') != 'Login'){
          redirect(base_url('index.php/credential/login'));
        }
        
      }

      public function index()
      {
        redirect(base_url('index.php/admin/title'));
      }

  }

?>