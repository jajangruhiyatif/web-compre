<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Service extends CI_Controller
  {

      public function __construct()
      {
        parent::__construct();
        $this->load->model('m_service');
        $this->load->helper('url');
        
        if($this->session->userdata('status') != 'Login'){
          redirect(base_url('index.php/credential/login'));
        }
      }

      public function index()
      {
        $data['service'] = $this->m_service->getAll();
        $this->load->view('admin/page/service', $data);
      }

      public function create()
      {
        $title = $this->input->post('title');
        $content = $this->input->post('content');

        $data = array(
          'title' => $title,
          'content' => $content,
          'date_created' => date("d/m/Y"),
          'date_changed' => date("d/m/Y")
          );
        $this->m_service->insert('master_service', $data);
        redirect(base_url('index.php/admin/service'));
      }
  }
?>