<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Testimony extends CI_Controller
  {

      public function __construct()
      {
        parent::__construct();
        $this->load->model('m_testimony');
        $this->load->helper('url');
        
        if($this->session->userdata('status') != 'Login'){
          redirect(base_url('index.php/credential/login'));
        }
      }

      public function index()
      {
        $data['testimony'] = $this->m_testimony->getAll();
        $this->load->view('admin/page/testimony', $data);
      }
  }
?>