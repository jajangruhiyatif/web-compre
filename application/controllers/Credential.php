<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Credential extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->model('m_user');
        $this->load->helper('url');
      }
      
      public function Login()
      {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
          'username' => $username,
          'password' => md5($password)
          );
        $check = $this->m_user->validate('user', $where)->num_rows();

        if($check > 0) {
          $data_session = array(
            'name' => $username,
            'status' => 'Login'
            );

          $this->session->set_userdata($data_session);

          echo "ok login";
          redirect(base_url('index.php/admin/home'));
        } else {
          echo "Invalid credential";
          redirect(base_url('index.php/login'));
        }
      }

      public function Logout()
      {
        $this->session->sess_destroy();
        redirect(base_url(''));
      }
  }
?>