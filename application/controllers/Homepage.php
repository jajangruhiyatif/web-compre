<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

  public function __construct() 
  {
    parent::__construct();
    $this->load->model('m_title');
    $this->load->helper('url');
  }

	public function index()
	{
    $data['data'] = $this->m_title->get_title();
		$this->load->view('homepage/index', $data);
	}

  public function title_data()
  {
    $data['title'] = $this->m_title->get_title(true);
    $this->load->view('homepage/index', $data);
  }

  public function login() 
  {
    $this->load->view('homepage/login');
  }
}
