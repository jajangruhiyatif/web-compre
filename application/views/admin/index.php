<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Content</title>
  <?php $this->load->view('admin/partial/_header'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('admin/partial/_navbar');?>
  <?php $this->load->view('admin/partial/_sidebar'); ?>
  <?php $this->load->view('admin/partial/_body'); ?>

  <?php $this->load->view('admin/page/index'); ?>
   
  <?php $this->load->view('admin/partial/_footer'); ?>
</body>
</html>