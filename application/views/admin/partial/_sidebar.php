  <aside class="main-sidebar">
    
    <section class="sidebar">
      
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/adminLTE/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat">
              <i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>

      <ul class="sidebar-menu" data-widget="tree">
                
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Master Content</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/admin/title'); ?>"><i class="fa fa-circle-o"></i>Title</a></li>
            <li><a href="<?php echo base_url('index.php/admin/service'); ?>"><i class="fa fa-circle-o"></i>Service</a></li>
            <li><a href="<?php echo base_url('index.php/admin/feature'); ?>"><i class="fa fa-circle-o"></i>Feature</a></li>
            <li><a href="<?php echo base_url('index.php/admin/testimony'); ?>"><i class="fa fa-circle-o"></i>Testimony</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Other</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/admin/contact_us'); ?>"><i class="fa fa-circle-o"></i>Contact Us</a></li>
            <li><a href="<?php echo base_url('index.php/admin/create'); ?>"><i class="fa fa-circle-o"></i>New Content</a></li>
          </ul>
        </li>
        
    </section>
  </aside>


  