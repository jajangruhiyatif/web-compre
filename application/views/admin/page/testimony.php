<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Master Testimony</title>
  <?php $this->load->view('admin/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('admin/partial/_navbar');?>
  <?php $this->load->view('admin/partial/_sidebar'); ?>
  <?php $this->load->view('admin/partial/_body'); ?>

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Master Testimony</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>Name</th>
              <th>Content</th>
              <th>Position</th>
              <th>Date Created</th>
              <th>Date Changed</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
              foreach ($testimony as $val) {
            ?>
            <tr>
              <td><?php echo $val->name ?></td>
              <td><?php echo $val->content ?></td>
              <td><?php echo $val->position ?></td>
              <td>
                <div class="sparkbar" data-color="#00a65a" data-height="20"><?php echo $val->date_created ?></div>
              </td>
              <td>
                <div class="sparkbar" data-color="#00a65a" data-height="20"><?php echo $val->date_changed ?></div>
              </td>
              <td>
                <a href="javascript:void(0)" class="btn btn-sm btn-warning btn-flat">Edit</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-flat">Delete</a>
              </td>
            </tr>
            <?php
              }
            ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="box-footer clearfix">
        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Create New</a>
      </div>
    </div>

  <?php $this->load->view('admin/partial/_footer'); ?>

<script>
 
</script>
</body>
</html>
