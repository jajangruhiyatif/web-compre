<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Master Services</title>
  <?php $this->load->view('admin/partial/_header'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('admin/partial/_navbar');?>
  <?php $this->load->view('admin/partial/_sidebar'); ?>
  <?php $this->load->view('admin/partial/_body'); ?>

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Master Services</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Content</th>
              <th>Date Created</th>
              <th>Date Changed</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
              $i=1;
              foreach ($service as $val) {
            ?>
            
            <tr>
              <td><?php echo $i ?></td>
              <td><?php echo $val->title ?></td>
              <td><?php echo $val->content ?></td>
              <td>
                <div class="sparkbar" data-color="#00a65a" data-height="20"><?php echo $val->date_created ?></div>
              </td>
              <td>
                <div class="sparkbar" data-color="#00a65a" data-height="20"><?php echo $val->date_changed ?></div>
              </td>
              <td>
                <button class="btn btn-sm btn-warning btn-flat btn-edit" data-toggle="modal" data-target="#modal-default">Edit</button>
                <button class="btn btn-sm btn-danger btn-flat" data-toggle="modal" data-target="#modal-default">Delete</button>
              </td>
            </tr>

            <?php
              $i++;
              }
            ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit Content</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Id</label>
                <input type="text" class="form-control" id="modal_id" placeholder="" disabled>
              </div>
              <div>
                <label>Title</label>
                <input type="text" class="form-control" id="modal_title" placeholder="">
              </div>
              <div>
                <label>Content</label>
                <textarea class="form-control" id="modal_content" rows="3" placeholder=""></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left btn-close" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <div class="box-footer clearfix">
        <button class="btn btn-sm btn-info btn-flat btn-edit" data-toggle="modal" data-target="#modal-default">Create</button>
      </div>

    </div>

  <?php $this->load->view('admin/partial/_footer'); ?>

<script>
  
  $("#modal-default").on('show.bs.modal', function(e) {
    $('#modal_id').val($('#id').val());
    $('#modal_title').val($('#title').val());
    $('#modal_content').val($('#content').val());
  });

  $(".btn-close").click(function() {
    $('#modal_id').val("");
    $('#modal_title').val("");
    $('#modal_content').val("");
  });

</script>
</body>
</html>
