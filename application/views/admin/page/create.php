<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Create Content</title>
  <?php $this->load->view('admin/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('admin/partial/_navbar');?>
  <?php $this->load->view('admin/partial/_sidebar'); ?>
  <?php $this->load->view('admin/partial/_body'); ?>

      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title">CK Editor
            <small>New content</small>
          </h3>
          
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
          
        </div>
        
        <div class="box-body pad">
          <form>
            <textarea id="editor1" name="editor1" rows="10" cols="80">
              This is my textarea to be replaced with CKEditor.
            </textarea>
          </form>
        </div>
      </div>
  <?php $this->load->view('admin/partial/_footer'); ?>

<script>
  $(function () {
    CKEDITOR.replace('editor1')

    $('.textarea').wysihtml5()
  })
</script>
</body>
</html>
