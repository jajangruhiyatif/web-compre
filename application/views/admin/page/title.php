<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Master Title</title>
  <?php $this->load->view('admin/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('admin/partial/_navbar');?>
  <?php $this->load->view('admin/partial/_sidebar'); ?>
  <?php $this->load->view('admin/partial/_body'); ?>

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Master Title</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Date Created</th>
              <th>Date Changed</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody id="data-title">
              
            </tbody>
          </table>
        </div>
      </div>

      <div class="modal fade" id="modal-create">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Save Content</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Title</label>
                <input type="text" class="form-control" id="add_title" placeholder=""><br>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-save" data-dismiss="modal">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit Content</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Title</label>
                <input type="text" class="form-control" id="add_title" placeholder=""><br>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-warning btn-edit" data-dismiss="modal">update change</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-delete" abindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Delete Content</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Title content</label>
                <input type="text" class="form-control" id="delete_title_content" placeholder="" disabled><br>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-delete" data-dismiss="modal">Delete</button>
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="box-footer clearfix">
        <button class="btn btn-sm btn-info btn-flat" data-toggle="modal" data-target="#modal-create">Create new</button>
      </div>
    </div>

  <?php $this->load->view('admin/partial/_footer'); ?>

<script>
  $(document).ready(function(){
    show_title();

    // get list
    function show_title() {
      $.ajax({
        type: 'GET',
        url: '<?php echo base_url('index.php/admin/title/show') ?>',
        async: true,
        dataType: 'json',
        success: function(data) {
          var html = "";
          var i = 0;
          for (var i = 0; i < data.length; i++) {
            html += 
              "<tr>\
                <td>"+(i+1)+"</td>\
                <td>"+data[i].title+"</td>\
                <td>\
                  <div class=\"sparkbar\" data-color=\"#00a65a\" data-height=\"20\">"+data[i].date_created+"</div>\
                </td>\
                <td>\
                  <div class=\"sparkbar\" data-color=\"#00a65a\" data-height=\"20\">"+data[i].date_changed+"</div>\
                </td>\
                <td>\
                  <button class=\"btn btn-sm btn-warning btn-flat\" data-toggle=\"modal\" data-target=\"#modal-edit\" data-title-id="+data[i].id+" data-title-content="+data[i].title+">Edit</button>\
                  <button class=\"btn btn-sm btn-danger btn-flat\" data-toggle=\"modal\" data-target=\"#modal-delete\" data-title-id="+data[i].id+" data-title-content="+data[i].title+">Delete</button>\
                </td>\
              </tr>"
          }
          $('#data-title').html(html);
        },
      });
    }
    
    // save
    $('.btn-save').on('click', function(e) {
      var button = $(e.relatedTarget);
      var txt_title = $('#add_title').val();
      var modal = $(this)

      $.ajax({
        type: 'POST',
        url: '<?php echo base_url('index.php/admin/title/save') ?>',
        data: { title: txt_title },
        success: function(data) {
          show_title();
          button.data('dismiss');
        }
      });
      return false;
    });

    $('#modal-edit').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget)
      var title_id = button.data('title-id');
      var title_content = button.data('title-content');

      var modal = $(this)
      modal.find('.modal-body input').val(title_content);

      $('.btn-edit').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('index.php/admin/title/test') ?>',
          data: { id: title_id },
          success: function() {
            show_title();
          },
        })
      });
    });

    // delete
    $('#modal-delete').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget)
      var title_id = button.data('title-id');
      var title_content = button.data('title-content');

      var modal = $(this)
      modal.find('.modal-body input').val(title_content);

      $('.btn-delete').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('index.php/admin/title/delete') ?>',
          data: { id: title_id },
          success: function() {
            show_title();
          },
        })
      });
    });

  });
</script>
</body>
</html>
