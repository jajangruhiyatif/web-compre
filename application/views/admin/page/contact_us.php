<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Contact Us</title>
  <?php $this->load->view('admin/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('admin/partial/_navbar');?>
  <?php $this->load->view('admin/partial/_sidebar'); ?>
  <?php $this->load->view('admin/partial/_body'); ?>

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Contact Us</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Content</th>
              <th>Date Created</th>
              <th>Date Changed</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>OR9842</td>
              <td>Call of Duty IV</td>
              <td>Shipped</td>
              <td>
                <div class="sparkbar" data-color="#00a65a" data-height="20">2019/11/11</div>
              </td>
              <td>
                <div class="sparkbar" data-color="#00a65a" data-height="20">2019/11/11</div>
              </td>
              <td>
                <a href="javascript:void(0)" class="btn btn-sm btn-warning btn-flat">Edit</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-danger btn-flat">Delete</a>
              </td>
            </tr>
            
            </tbody>
          </table>
        </div>
      </div>
      <div class="box-footer clearfix">
        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Create New</a>
        <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Content</a>
      </div>
    </div>

  <?php $this->load->view('admin/partial/_footer'); ?>

<script>
  
</script>
</body>
</html>
