<?php
	$this->load->helper("url");
?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/fav.png">
		
		<meta name="author" content="codepixer">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta charset="UTF-8">
		<!-- <title>Plumber</title> -->
    <title><?php echo $data->title?></title>


			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linearicons.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hexagons.min.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
		</head>
		<body>

		  <header id="header" id="home">
		    <div class="container">
		    	<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <!-- <a href="index.php"><img src="<?php echo base_url(); ?>assets/img/logo.png" alt="" title="" /></a> -->
              <h4 class="text-white text-uppercase">
                ~ <?php echo $data->title?> ~
              </h4>
			      </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu">
			          <li class="menu-active"><a href="#home">Home</a></li>
			          <li><a href="#service">Services</a></li>
			          <li><a href="#feature">Features</a></li>
			          <li><a href="#testimonail">Testimonail</a></li>
			          <li><a href="#contact">Contact</a></li>
			          <li class="menu-has-children"><a href="">Pages</a>
			            <ul>
			              <li><a href="index.php/login">Login</a></li>
			            </ul>
			          </li>
			        </ul>
			      </nav>
		    	</div>
		    </div>
		  </header>


			
			<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-start">
						<div class="banner-content col-lg-9 col-md-12">
							<h1 class="text-white text-uppercase">
								<?php echo $data->title?>
							</h1>
							<p class="text-white">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.  sed do eiusmod tempor incididunt.
							</p>
							<a href="#" class="primary-btn text-uppercase">buy now for $9.99</a>
						</div>											
					</div>
				</div>
			</section>
			
			<section class="quote-area pt-100">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-5 col-sm-12 quote-left">
							<h2 class="text-right">
								<span>Plumbing</span> for those Area<br>
								where like to change<br>
								<span>Nowhere</span>.
							</h2>
						</div>
						<div class="col-lg-7 col-sm-12 quote-right">
							<p class="text-left">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
							</p>
						</div>
					</div>
				</div>	
			</section>
			

			
			<section class="cat-area section-gap" id="service">
				<div class="container">							
					<div class="row">
						<div class="col-lg-4">	
							<div class="single-cat d-flex flex-column">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-magic-wand"></span></span></a>
								<h4 class="mb-20" style="margin-top: 23px;">Maintenance</h4>
								<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why.
								</p>
							</div>															
						</div>
						<div class="col-lg-4">	
							<div class="single-cat">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-rocket"></span></span></a>
								<h4 class="mt-40 mb-20">Residental Service</h4>
								<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why.
								</p>
							</div>															
						</div>
						<div class="col-lg-4">
							<div class="single-cat">
								<a href="#" class="hb-sm-margin mx-auto d-block"><span class="hb hb-sm inv hb-facebook-inv"><span class="lnr lnr-bug"></span></span></a>
								<h4 class="mt-40 mb-20">Commercial Service</h4>
								<p>
									inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why.
								</p>
							</div>							
						</div>
					</div>
				</div>	
			</section>
			
					
			
			<section class="feature-area relative" id="feature">
				<img class="feature-img" src="<?php echo base_url(); ?>assets/img/f1.jpg" alt="">
				<div class="container">				
					<div class="row align-items-center">
						<div class="col-lg-8 offset-lg-4 feature-right section-gap">
							<div class="row single-feature-wrap justify-content-end">
								<div class="title-wrap col-lg-10 col-sm-12">
									<h1>Features That make us Unique</h1>
									<p>
										Who are in extremely love with eco friendly system.
									</p>									
								</div>

								<div class="col-lg-5">
									<div class="single-feature">
										<h4>Multiple Layouts</h4>
										<p>
											Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization.
										</p>
									</div>
									<div class="single-feature">
										<h4>Clean Coding</h4>
										<p>
											Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization.
										</p>
									</div>								
								</div>
								<div class="col-lg-5">
									<div class="single-feature">
										<h4>Endless Features</h4>
										<p>
											Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization.
										</p>
									</div>
									<div class="single-feature">
										<h4>Fully Customizable</h4>
										<p>
											Usage of the Internet is becoming more common due to rapid advancement of technology and the power of globalization.
										</p>
									</div>								
								</div>									
							</div>						
						</div>
					</div>
				</div>	
			</section>
			
							
			
			<section class="counter-area section-gap" id="counter">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-6">
							<div class="single-counter">
								<h1 class="counter">2536</h1>
								<p>Happy Clients</p>								
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-counter">
								<h1 class="counter">6784</h1>
								<p>Total Projects</p>								
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-counter">
								<h1 class="counter">1059</h1>
								<p>Cups Coffee</p>								
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="single-counter">
								<h1 class="counter">12239</h1>
								<p>Tickets Submitted</p>								
							</div>
						</div>												
					</div>
				</div>	
			</section>
			

			
			<section class="video-area pb-100" id="video">
				<div class="container">
					<div class="row justify-content-center">
						<div class="video-section section-gap col-lg-12">
							<div class="text-wrap">
								<h1 class="text-white">Explore ourself in a new way</h1>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore <br> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
								</p>
								<div class="play-btn-wrap">
									<a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="play-btn">
									
									<img src="<?php echo base_url(); ?>assets/img/play-btn.png" alt=""> Watch Video
									</a>
								</div>								
							</div>
						</div>
					</div>
				</div>	
			</section>
			
															
			
			<section class="callto-action-area section-gap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-9">
							<h1 class="text-white">Got Impressed to our features</h1>
							<p class="text-white">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
							</p>
							<a class="primary-btn" href="#">Get a free Quote</a>							
						</div>
					</div>
				</div>	
			</section>
			


			
			<section class="testomial-area section-gap" id="testimonail">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">What our Client’s Say about us</h1>
								<p>Who are in extremely love with eco friendly system.</p>
							</div>
						</div>
					</div>						
					<div class="row">
						<div class="active-tstimonial-carusel">
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t1.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t2.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t3.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>	
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t1.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t2.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t3.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>															
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t1.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t2.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>
							<div class="single-testimonial item">
								<img class="mx-auto" src="<?php echo base_url(); ?>assets/img/t3.png" alt="">
								<p class="desc">
									Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory
								</p>
								<h4>Mark Alviro Wiens</h4>
								<p>
									CEO at Google
								</p>
							</div>														
						</div>
					</div>
				</div>	
			</section>
			

			
			<section class="contact-area" id="contact">
				<div class="container">
					<div class="contact-section">
						<div class="row align-items-center">
							<div class="col-lg-4 contact-left">
								<div style=" width:100%;height: 545px;" id="map"></div>
							</div>
							<div class="col-lg-7 contact-right">
								<form class="form-area" id="myForm" action="mail.php" method="post" class="contact-form text-right">
									<input name="fname" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mt-10" required="" type="text">
									<input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mt-10" required="" type="email">
									<textarea class="common-textarea mt-10" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
									<button class="primary-btn mt-20">Send Message<span class="lnr lnr-arrow-right"></span></button>
									<div class="alert-msg">
									</div>
								</form>
							</div>							
						</div>
					</div>
				</div>	
			</section>
			
			
			
			
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>About Us</h6>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.
								</p>
								<p class="footer-text">
									
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> Re-distributed by <a target="_blank" href="www.Themewagon.com">Themewagon</a>

								</p>								
							</div>
						</div>
						<div class="col-lg-5  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Newsletter</h6>
								<p>Stay update with our latest</p>
								<div class="" id="mc_embed_signup">
									<form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">
										<input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
			                            	<button class="click-btn btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
			                            	<div style="position: absolute; left: -5000px;">
												<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
											</div>

										<div class="info"></div>
									</form>
								</div>
							</div>
						</div>						
						<div class="col-lg-2 col-md-6 col-sm-6 social-widget">
							<div class="single-footer-widget">
								<h6>Follow Us</h6>
								<p>Let us be social</p>
								<div class="footer-social d-flex align-items-center">
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-dribbble"></i></a>
									<a href="#"><i class="fa fa-behance"></i></a>
								</div>
							</div>
						</div>							
					</div>
				</div>
			</footer>	
			

			<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min.js"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="<?php echo base_url(); ?>assets/js/easing.min.js"></script>			
			<script src="<?php echo base_url(); ?>assets/js/hoverIntent.js"></script>
			<script src="<?php echo base_url(); ?>assets/js/superfish.min.js"></script>	
			<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>	
			<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>			
			<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>
			<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>	
			<script src="<?php echo base_url(); ?>assets/js/hexagons.min.js"></script>					
			<script src="<?php echo base_url(); ?>assets/js/waypoints.min.js"></script>			
			<script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>						
			<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>	
			<script src="<?php echo base_url(); ?>assets/js/main.js"></script>	
		</body>
	</html>



