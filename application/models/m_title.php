<?php defined('BASEPATH') OR exit('No direct script access allowed');
  
  class m_title extends CI_Model
  {
    
    public function list_title()
    {
      return $this->db->get('master_title')->result();
    }
    
    public function get_title()
    {
      $data = $this->db->get_where('master_title')->row();
      return $data;
    }

    public function save_title()
    {
      $data = array(
            'title'        => $this->input->post('title'),
            'date_created' => date("d/m/Y"),
            'date_changed' => date("d/m/Y")
          );
      $result = $this->db->insert('master_title', $data);
      return $result;
    }

    public function update_title()
    {
      $title_id = $this->input->post('id');
      $title    = $this->input->post('title');
      $dt_changed = date('d/m/Y');

      $this->db->set('title', $title);
      $this->db->set('date_changed', $dt_changed);
      $this->db->where('id', $title_id);
      $result = $this->db->update('master_title');
      return $result;
    }

    public function delete_title()
    {
      $title_id = $this->input->post('id');
      $this->db->where('id', $title_id);
      $result = $this->db->delete('master_title');
      return $result;
    }

  }

?>