-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `webcompre`;
CREATE DATABASE `webcompre` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `webcompre`;

DROP TABLE IF EXISTS `master_contact`;
CREATE TABLE `master_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(35) NOT NULL,
  `message` text NOT NULL,
  `date_created` varchar(10) DEFAULT NULL,
  `date_changed` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `master_contact` (`id`, `name`, `email`, `message`, `date_created`, `date_changed`) VALUES
(1, 'test contact', 'rdz@gmail.com',  'asd',  '11/12/2019', '11/12/2019')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `name` = VALUES(`name`), `email` = VALUES(`email`), `message` = VALUES(`message`), `date_created` = VALUES(`date_created`), `date_changed` = VALUES(`date_changed`);

DROP TABLE IF EXISTS `master_feature`;
CREATE TABLE `master_feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `date_created` varchar(10) DEFAULT NULL,
  `date_changed` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `master_feature` (`id`, `title`, `content`, `date_created`, `date_changed`) VALUES
(1, 'test feature', 'asd',  '11/12/2019', '11/12/2019'),
(2, 'test feature', 'test data',  '11/12/2019', '11/12/2019')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `title` = VALUES(`title`), `content` = VALUES(`content`), `date_created` = VALUES(`date_created`), `date_changed` = VALUES(`date_changed`);

DROP TABLE IF EXISTS `master_service`;
CREATE TABLE `master_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `date_created` varchar(10) DEFAULT NULL,
  `date_changed` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `master_service` (`id`, `title`, `content`, `date_created`, `date_changed`) VALUES
(1, 'test data',  'asd',  '11/12/2019', '11/12/2019'),
(2, 'test serv',  'aasd', '11/12/2019', '11/12/2019')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `title` = VALUES(`title`), `content` = VALUES(`content`), `date_created` = VALUES(`date_created`), `date_changed` = VALUES(`date_changed`);

DROP TABLE IF EXISTS `master_testimony`;
CREATE TABLE `master_testimony` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `position` varchar(30) DEFAULT NULL,
  `date_created` varchar(10) DEFAULT NULL,
  `date_changed` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `master_testimony` (`id`, `name`, `content`, `position`, `date_created`, `date_changed`) VALUES
(1, 'test testi', 'asd',  'Director', '11/12/2019', '11/12/2019')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `name` = VALUES(`name`), `content` = VALUES(`content`), `position` = VALUES(`position`), `date_created` = VALUES(`date_created`), `date_changed` = VALUES(`date_changed`);

DROP TABLE IF EXISTS `master_title`;
CREATE TABLE `master_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(15) NOT NULL,
  `date_created` varchar(10) NOT NULL,
  `date_changed` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO `master_title` (`id`, `title`, `date_created`, `date_changed`) VALUES
(13,  'New World',  '07/09/2019', '07/09/2019')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `title` = VALUES(`title`), `date_created` = VALUES(`date_created`), `date_changed` = VALUES(`date_changed`);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`username`, `password`) VALUES
('admin', '827ccb0eea8a706c4c34a16891f84e7b')
ON DUPLICATE KEY UPDATE `username` = VALUES(`username`), `password` = VALUES(`password`);

-- 2019-09-06 21:18:34